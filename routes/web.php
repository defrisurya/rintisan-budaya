<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


/* Dashboard */
Route::get('rkb', 'front\frontController@index')->name('rkb');
Route::get('profile', 'front\profileController@index')->name('profile');
Route::get('gallerykegiatan', 'front\GalleryController@index');
Route::get('struktur', 'front\StrukturController@index')->name('struktur');
Route::get('kegiatanrutin/{id}', 'front\KegiatanController@info')->name('info');

Auth::routes([
    'register' => false,
    'reset' => false,
    'verify' => false,
]);

Route::group(['middleware' => ['auth']], function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::resource('profil', 'Admin\profilController');
    Route::resource('gallery', 'Admin\galleryController');
    Route::resource('kegiatan', 'Admin\kegiatanController');
    Route::resource('fotostruktur', 'Admin\fotoStrukturController');
    Route::resource('detailstruktur', 'Admin\detailStrukturController');
});
