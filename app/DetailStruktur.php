<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailStruktur extends Model
{
    protected $fillable = ['foto', 'deskripsi'];
}
