<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profil extends Model
{
    protected $table = 'profils';
    protected $fillable = [
        'foto',
        'alamat',
        'notelpon',
        'email',
        'ytb',
        'ig',
        'fb',
        'longitude',
        'latitude',
        'video',
        'deskripsi',
    ];
}
