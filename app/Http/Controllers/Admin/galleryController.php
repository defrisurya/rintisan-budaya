<?php

namespace App\Http\Controllers\Admin;

use App\Gallery;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\createGalleryRequest;
use App\Http\Requests\Admin\updateGalleryRequest;
use Illuminate\Http\Request;

class galleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Gallery::paginate(10);
        return view('Admin.Gallery.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Admin.Gallery.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(createGalleryRequest $request)
    {
        $data = $request->all();
        if($request->has('foto')){
            $foto = $request->foto;
            $new_foto = time() . 'gallery' . $foto->getClientOriginalName();
            $tujuan_uploud = 'uplouds/gallery/';
            $foto->move($tujuan_uploud, $new_foto);
            $data['foto'] = $tujuan_uploud . $new_foto;
        }
        Gallery::create($data);
        toast('Data Gallery Berhasil Di Tambah','success');
        return redirect()->route('gallery.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Gallery $gallery)
    {
        return view('Admin.Gallery.edit', compact('gallery'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(updateGalleryRequest $request, Gallery $gallery)
    {
        $data = $request->all();
        $data['foto'] = $gallery->foto;
        if($request->has('foto')){
            $foto = $request->foto;
            $new_foto = time() . 'gallery' . $foto->getClientOriginalName();
            $tujuan_uploud = 'uplouds/gallery/';
            $foto->move($tujuan_uploud, $new_foto);
            $data['foto'] = $tujuan_uploud . $new_foto;
        }
        $gallery->update($data);
        toast('Data Gallery Berhasil Di Update','success');
        return redirect()->route('gallery.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Gallery $gallery)
    {
        $gallery->delete();
        toast('Data Gallery Berhasil Di Hapus','error');
        return redirect()->route('gallery.index');
    }
}
