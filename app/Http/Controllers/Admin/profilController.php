<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Profil;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Str;



class profilController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $profil = Profil::first();
        // dd($profil);
        return view('Admin.Profile.index', compact('profil'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Profil $profil)
    {
        return view('Admin.Profile.edit', compact('profil'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Profil $profil)
    {
        $data = $request->all();
        $data['foto'] = $profil->foto;
        $data['video'] = $profil->video;
        if($request->has('foto')){
            $foto = $request->foto;
            $new_foto = time() . 'profil' . $foto->getClientOriginalName();
            $tujuan_uploud = 'uplouds/profil/';
            $foto->move($tujuan_uploud, $new_foto);
            $data['foto'] = $tujuan_uploud . $new_foto;
        }

        $auto = "?autoplay=1";
            $url = $request->video;
            $jml1 = strlen($url);
            if($jml1 == 52)
            {
                $youtube_inti = $url;
            }else{
                $url_limit = substr($url, 0, 43);
                $youtube_url = Str::of($url_limit)->replace("watch?v=", "embed/");
                $fix_url = Str::before($youtube_url, '?autoplay=1');
                $youtube_inti = $fix_url . "" . $auto;
            }

        $data['video'] = $youtube_inti;

        $profil->update($data);
        toast('Data Profil Berhasil Di Update','success');
        return redirect()->route('profil.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
