<?php

namespace App\Http\Controllers\Admin;

use App\DetailStruktur;
use App\FotoStruktur;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class fotoStrukturController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fotostruktur = FotoStruktur::first();
        return view('Admin.Struktur.index', compact('fotostruktur'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(FotoStruktur $fotostruktur)
    {
        return view('Admin.Struktur.edit', compact('fotostruktur'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FotoStruktur $fotostruktur)
    {
        $data['foto'] = $fotostruktur->foto;
        if($request->has('foto')){
            $foto = $request->foto;
            $new_foto = time() . 'fotostruktur' . $foto->getClientOriginalName();
            $tujuan_uploud = 'uplouds/fotostruktur/';
            $foto->move($tujuan_uploud, $new_foto);
            $data['foto'] = $tujuan_uploud . $new_foto;
        }
        $fotostruktur->update($data);
        toast('Foto Struktur Berhasil Di Update','success');
        return redirect()->route('fotostruktur.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
