<?php

namespace App\Http\Controllers\Admin;

use App\DetailStruktur;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\detailStrukturCreate;
use App\Http\Requests\Admin\detailStrukturUpdate;
use Illuminate\Http\Request;

class detailStrukturController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DetailStruktur::paginate(10);
        return view('Admin.DetailStruktur.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Admin.DetailStruktur.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(detailStrukturCreate $request)
    {
        $data = $request->all();
        if($request->has('foto')){
            $foto = $request->foto;
            $new_foto = time() . 'anggota' . $foto->getClientOriginalName();
            $tujuan_uploud = 'uplouds/anggota/';
            $foto->move($tujuan_uploud, $new_foto);
            $data['foto'] = $tujuan_uploud . $new_foto;
        }
        DetailStruktur::create($data);
        toast('Data Anggota Berhasil Di Tambah','success');
        return redirect()->route('detailstruktur.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(DetailStruktur $detailstruktur)
    {
        return view('Admin.DetailStruktur.edit', compact('detailstruktur'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(detailStrukturUpdate $request, DetailStruktur $detailstruktur)
    {
        $data = $request->all();
        $data['foto'] = $detailstruktur->foto;
        if($request->has('foto')){
            $foto = $request->foto;
            $new_foto = time() . 'anggota' . $foto->getClientOriginalName();
            $tujuan_uploud = 'uplouds/anggota/';
            $foto->move($tujuan_uploud, $new_foto);
            $data['foto'] = $tujuan_uploud . $new_foto;
        }
        $detailstruktur->update($data);
        toast('Data Anggota Berhasil Di Update','success');
        return redirect()->route('detailstruktur.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(DetailStruktur $detailstruktur)
    {
        $detailstruktur->delete();
        toast('Data Anggota Berhasil Di Hapus','error');
        return redirect()->route('detailstruktur.index');
    }
}
