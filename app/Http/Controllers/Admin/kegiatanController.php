<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\kegiatanCreateRequest;
use App\Http\Requests\Admin\kegiatanUpdateRequest;
use App\Kegiatan;
use Illuminate\Http\Request;

class kegiatanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Kegiatan::paginate(10);
        return view('Admin.Kegiatan.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Admin.Kegiatan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(kegiatanCreateRequest $request)
    {
        $data = $request->all();
        if($request->has('foto')){
            $foto = $request->foto;
            $new_foto = time() . 'kegiatan' . $foto->getClientOriginalName();
            $tujuan_uploud = 'uplouds/kegiatan/';
            $foto->move($tujuan_uploud, $new_foto);
            $data['foto'] = $tujuan_uploud . $new_foto;
        }
        Kegiatan::create($data);
        toast('Data Kegiatan Berhasil Di Tambah','success');
        return redirect()->route('kegiatan.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Kegiatan $kegiatan)
    {
        return view('Admin.Kegiatan.edit', compact('kegiatan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(kegiatanUpdateRequest $request, Kegiatan $kegiatan)
    {
        $data = $request->all();
        $data['foto'] = $kegiatan->foto;
        if($request->has('foto')){
            $foto = $request->foto;
            $new_foto = time() . 'kegiatan' . $foto->getClientOriginalName();
            $tujuan_uploud = 'uplouds/kegiatan/';
            $foto->move($tujuan_uploud, $new_foto);
            $data['foto'] = $tujuan_uploud . $new_foto;
        }
        $kegiatan->update($data);
        toast('Data Kegiatan Berhasil Di Update','success');
        return redirect()->route('kegiatan.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Kegiatan $kegiatan)
    {
        $kegiatan->delete();
        toast('Data Kegiatan Berhasil Di Hapus','error');
        return redirect()->route('kegiatan.index');
    }
}
