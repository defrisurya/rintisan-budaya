<?php

namespace App\Http\Controllers\front;

use App\Http\Controllers\Controller;
use App\Profil;
use Illuminate\Http\Request;

class profileController extends Controller
{
    public function index()
    {
        $prof = Profil::first();
        return view('front.profile.profile', compact('prof'));
    }
}
