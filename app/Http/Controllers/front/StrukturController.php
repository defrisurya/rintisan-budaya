<?php

namespace App\Http\Controllers\front;

use App\DetailStruktur;
use App\FotoStruktur;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class StrukturController extends Controller
{
    public function index()
    {
        $struk = DetailStruktur::all();
        $fotostruk = FotoStruktur::first();
        return view('front.struktur.struktur', compact('struk', 'fotostruk'));
    }
}
