<?php

namespace App\Http\Controllers\front;

use App\Http\Controllers\Controller;
use App\Kegiatan;
use Illuminate\Http\Request;

class KegiatanController extends Controller
{
    public function info($id)
    {
        $action = Kegiatan::find($id);
        return view('front.kegiatan.kegiatan', compact('action'));
    }
}
