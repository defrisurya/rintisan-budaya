<?php

namespace App\Http\Controllers\front;

use App\Gallery;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class GalleryController extends Controller
{
    public function index()
    {
        $galeri = Gallery::all();
        return view('front.gallery.allGallery', compact('galeri'));
    }
}
