<?php

namespace App\Http\Controllers\front;

use App\Http\Controllers\Controller;
use App\Profil;
use App\Kegiatan;
use App\Gallery;
use Illuminate\Http\Request;

class frontController extends Controller
{
    public function index()
    {
        $rkb = Profil::first();
        $kr = Kegiatan::all();
        $gal = Gallery::all();
        return view('front.dashboard', compact('rkb', 'kr', 'gal'));
    }
}
