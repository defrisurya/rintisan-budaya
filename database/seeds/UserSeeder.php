<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => Str::random(10),
            'email' => 'admin@mail.com',
            'role' => 'Admin',
            'password' => Hash::make('password'),
        ]);

        DB::table('profils')->insert([
            'foto' => 'foto',
            'alamat' => 'Jl Tasuran',
            'notelpon' => '08217673467',
            'email' => 'profils@mail.com',
            'ytb' => 'https',
            'ig' => 'https',
            'fb' => 'https',
            'longitude' => '12321',
            'latitude' => '23132',
            'video' => 'https',
            'deskripsi' => 'tes deskripsi',
        ]);

        DB::table('foto_strukturs')->insert([
            'foto' => 'foto'
        ]);
    }
}
