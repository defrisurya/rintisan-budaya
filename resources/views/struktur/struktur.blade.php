@extends('layouts.app')

@section('title', 'Struktur Organisasi')

@section('content')
    <div id="preloader"></div>

    <!-- Navbar -->
    <header>
        <div id="sticker" class="header-area" style="background-color: rgba(0, 0, 0, 0.74)">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <nav class="navbar navbar-default" data-aos="fade-down" data-aos-duration="1200">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".bs-example-navbar-collapse-1" aria-expanded="false">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <a class="navbar-brand page-scroll sticky-logo" href="{{ route('welcome') }}">
                                    <img src="{{asset('image/Rectangle1.png')}}">
                                    <span>Dinas Kebudayaan (Kundha Kabudayan)<br>&nbsp;&nbsp;Kota Yogyakarta</span>
                                </a>
                            </div>
                            <div class="collapse navbar-collapse main-menu bs-example-navbar-collapse-1" id="navbar-example">
                                <ul class="nav navbar-nav navbar-right">
                                    <li>
                                        <a href="{{ route('welcome') }}">Home</a>
                                    </li>
                                    <li class="dropdown active"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Profiles&nbsp;<span class="caret"></span></a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="{{ route('profile') }}">Profil Kampung</a></li>
                                            <li><a href="{{ route('struktur') }}">Struktur Organisasi</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="{{ route('gallery') }}">Gallery</a>
                                    </li>
                                    <li>
                                        <a href="#contact">Contact</a>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- End Navbar -->

    <!-- Struktur Organisasi -->
    <div id="about" class="about-area area-padding">
        <div class="container">
            <div class="row" data-aos="fade-down" data-aos-duration="1200">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="section-headline text-center">
                        <h2><img class="group_11" src="{{ asset('image/Group_11.png') }}">&nbsp;&nbsp;Struktur Organisasi</h2>
                    </div>
                </div>
            </div>
            <div class="row" data-aos="zoom-in" data-aos-duration="1200" data-aos-delay="900">
                <div class="col-md-12 col-sm-6 col-xs-12">
                    <div class="well-left">
                        <img src="{{ asset('image/struktur_org.png') }}">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Struktur Organisasi -->

    <!-- Detail Struktur Organisasi -->
    <div id="team" class="our-team-area area-padding reveal">
        <div class="container">
            <div class="row" data-aos="fade-down" data-aos-duration="1200">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="section-headline text-center">
                        <h2><img class="group_13" src="{{ asset('image/Group_11.png') }}">&nbsp;Detail Struktur Organisasi</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="team-top" data-aos="zoom-out" data-aos-delay="900">
                    <div class="col-md-3 col-sm-3 col-xs-12 p-4">
                        <div class="team-member-border">
                            <div class="img-team">
                                <img src="{{ asset('image/poster.png') }}" alt="">
                            </div>
                            <div class="team-content">
                                <p>Nama Lengkap</p>
                                <p style="font-weight: bold">Mr. Sudibyo</p>
                                <p>Jabatan</p>
                                <p style="font-weight: bold">Ketua</p>
                                <p>Periode Kepengurusan</p>
                                <p style="font-weight: bold">12 Januari 2021 - 1 Desember 2021</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-12 p-4">
                        <div class="team-member-border">
                            <div class="img-team">
                                <img src="{{ asset('image/poster.png') }}" alt="">
                            </div>
                            <div class="team-content">
                                <p>Nama Lengkap</p>
                                <p style="font-weight: bold">Mr. Munarwan</p>
                                <p>Jabatan</p>
                                <p style="font-weight: bold">Wakil Ketua</p>
                                <p>Periode Kepengurusan</p>
                                <p style="font-weight: bold">12 Januari 2021 - 1 Desember 2021</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-12 p-4">
                        <div class="team-member-border">
                            <div class="img-team">
                                <img src="{{ asset('image/poster.png') }}" alt="">
                            </div>
                            <div class="team-content">
                                <p>Nama Lengkap</p>
                                <p style="font-weight: bold">Mr. Subiman</p>
                                <p>Jabatan</p>
                                <p style="font-weight: bold">Sekretaris</p>
                                <p>Periode Kepengurusan</p>
                                <p style="font-weight: bold">12 Januari 2021 - 1 Desember 2021</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-12 p-4">
                        <div class="team-member-border">
                            <div class="img-team">
                                <img src="{{ asset('image/poster.png') }}" alt="">
                            </div>
                            <div class="team-content">
                                <p>Nama Lengkap</p>
                                <p style="font-weight: bold">Mr. Faturahman</p>
                                <p>Jabatan</p>
                                <p style="font-weight: bold">Bendahara</p>
                                <p>Periode Kepengurusan</p>
                                <p style="font-weight: bold">12 Januari 2021 - 1 Desember 2021</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-12 p-4">
                        <div class="team-member-border">
                            <div class="img-team">
                                <img src="{{ asset('image/poster.png') }}" alt="">
                            </div>
                            <div class="team-content">
                                <p>Nama Lengkap</p>
                                <p style="font-weight: bold">Mr. Rahman</p>
                                <p>Jabatan</p>
                                <p style="font-weight: bold">Seksi Budaya</p>
                                <p>Periode Kepengurusan</p>
                                <p style="font-weight: bold">12 Januari 2021 - 1 Desember 2021</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Detail Struktur Organisasi -->

    <!-- Footer -->
    @include('layouts.components.footer')
@endsection
@section('script')
    <script type="text/javascript">
        /*AOS
        --------------------- */
        AOS.init();
    </script>
@endsection
