@extends('layouts.purple')

@section('content')
  <div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">
            <div class="row mb-4">
                <div class="col-md-6">
                    <h4 class="card-title">Foto Struktur</h4>
                </div>
            </div>
            <table class="table table-hover text-center">
              <thead>
                <tr>
                  <th> No </th>
                  <th> Foto </th>
                  <th> Aksi </th>
                </tr>
              </thead>
              <tbody>
                 <tr>
                     <td>1</td>
                     <td>
                        <img src="{{ asset($fotostruktur->foto) }}" style="border-radius: 0; width: 150px; height: 100px;" alt="">
                     </td>
                     <td>
                        <a class="btn btn-gradient-warning btn-sm" href="{{ route('fotostruktur.edit', $fotostruktur) }}"><i class="mdi mdi-file-check btn-icon-append"></i></a>
                     </td>
                 </tr>
                
              </tbody>
            </table>
          </div>
        </div>
      </div>
      
  </div>
    
@endsection