@extends('layouts.purple')

@section('content')
<div class="row" id="proBanner">
    {{-- <div class="col-12">
      <span class="d-flex align-items-center purchase-popup">
        <p>Get tons of UI components, Plugins, multiple layouts, 20+ sample pages, and more!</p>
        <a href="https://www.bootstrapdash.com/product/purple-bootstrap-admin-template?utm_source=organic&utm_medium=banner&utm_campaign=free-preview" target="_blank" class="btn download-button purchase-button ml-auto">Upgrade To Pro</a>
        <i class="mdi mdi-close" id="bannerClose"></i>
      </span>
    </div> --}}
  </div>
  {{-- <div class="page-header">
    <h3 class="page-title">
      <span class="page-title-icon bg-gradient-primary text-white mr-2">
        <i class="mdi mdi-account-box"></i>
      </span> Profile
    </h3>
    <nav aria-label="breadcrumb">
      <ul class="breadcrumb">
        <li class="breadcrumb-item active" aria-current="page">
          <span></span>Overview <i class="mdi mdi-alert-circle-outline icon-sm text-primary align-middle"></i>
        </li>
      </ul>
    </nav>
  </div> --}}

  <div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">
            <h4 class="card-title">Profile RKB</h4> <br>
            <table class="table table-hover text-center">
              <thead>
                <tr>
                  <th> No </th>
                  <th> Foto </th>
                  <th> Email </th>
                  <th> Telpone </th>
                  <th> Alamat </th>
                  <th> Aksi </th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td class="py-1">
                    1
                  </td>
                  <td> <img src="{{ asset($profil->foto) }}" style="border-radius: 0; width: 150px; height: 100px;" alt=""> </td>
                  <td>
                      {{ $profil->email }}  
                  </td>
                  <td> {{ $profil->notelpon }} </td>
                  <td> {{ \Illuminate\Support\Str::limit($profil->alamat, 30, $end='...') }} </td>
                  <td> <a href="{{ route('profil.edit', $profil) }}" class="btn btn-gradient-primary btn-sm">Edit <i class="mdi mdi-file-check btn-icon-append"></i></a> </td>
                </tr>
                
              </tbody>
            </table>
          </div>
        </div>
      </div>
      
  </div>
    
@endsection