@extends('layouts.purple')

@section('content')
  {{-- <div class="page-header">
    <h3 class="page-title">
      <span class="page-title-icon bg-gradient-primary text-white mr-2">
        <i class="mdi mdi-account-box"></i>
      </span> Edit Profile
    </h3>
    <nav aria-label="breadcrumb">
      <ul class="breadcrumb">
        <li class="breadcrumb-item active" aria-current="page">
          <span></span>Overview <i class="mdi mdi-alert-circle-outline icon-sm text-primary align-middle"></i>
        </li>
      </ul>
    </nav>
  </div> --}}

  <div class="row">
    <div class="col-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">
            <h4 class="card-title">EDIT PROFIL</h4> <br>
            {{-- <p class="card-description"> Basic form elements </p> --}}
            <form class="forms-sample" action="{{ route('profil.update', $profil) }}" method="POST" enctype="multipart/form-data">
                @method('PUT')
                @csrf
              <div class="form-group">
                <label for="exampleInputEmail3">Email</label>
                <input type="email" class="form-control" name="email" id="exampleInputEmail3" placeholder="Email" value="{{ old('email', $profil->email) }}">
              </div>
              <div class="form-group">
                <label for="exampleInputName1">Telpon</label>
                <input type="number" name="notelpon" class="form-control" id="exampleInputName1" placeholder="Nomer Telpon" value="{{ old('notelpon', $profil->notelpon) }}">
              </div>
              <div class="form-group">
                <label for="exampleTextarea1">Alamat</label>
                <textarea class="form-control" id="exampleTextarea1" name="alamat" rows="4">{{ old('alamat', $profil->alamat) }}</textarea>
              </div>
              <div class="form-group">
                <label for="exampleInputName1">Youtube</label>
                <input type="text" name="ytb" class="form-control" id="exampleInputName1" placeholder="Youtube" value="{{ old('ytb', $profil->ytb) }}">
              </div>
              <div class="form-group">
                <label for="exampleInputName1">Instagram</label>
                <input type="text" name="ig" class="form-control" id="exampleInputName1" placeholder="Instagram" value="{{ old('ig', $profil->ig) }}">
              </div>
              <div class="form-group">
                <label for="exampleInputName1">Facebook</label>
                <input type="text" name="fb" class="form-control" id="exampleInputName1" placeholder="Facebook" value="{{ old('fb', $profil->fb) }}">
              </div>
              <div class="form-group">
                <label for="exampleInputName1">Latitude</label>
                <input type="number" name="latitude" class="form-control" id="exampleInputName1" placeholder="latitude" value="{{ old('latitude', $profil->latitude) }}">
              </div>
              <div class="form-group">
                <label for="exampleInputName1">Longitude</label>
                <input type="number" name="longitude" class="form-control" id="exampleInputName1" placeholder="longitude" value="{{ old('longitude', $profil->longitude) }}">
              </div>
              <div class="form-group">
                <label for="exampleTextarea1">Deskripsi</label>
                <textarea class="form-control" name="deskripsi" id="exampleTextarea1" rows="4">{{ old('deskripsi', $profil->deskripsi) }}</textarea>
              </div>              
              <div class="form-group">
                <label>File upload</label>
                <input type="file" name="foto" class="file-upload-default">
                <div class="input-group col-xs-12">
                  <input type="text" class="form-control file-upload-info" disabled="" placeholder="Upload Image">
                  <span class="input-group-append">
                    <button class="file-upload-browse btn btn-gradient-primary" type="button">Upload</button>
                  </span>
                </div>
                <div class="form-group mt-2" style="max-width: 20rem;">
                    <img src="{{ asset($profil->foto) }}" class="img-fluid" alt="">
                </div>
              </div>
              <div class="form-group">
                <label for="exampleInputName1">Video</label>
                <input type="text" name="video" class="form-control" id="exampleInputName1" placeholder="video" value="{{ old('video', $profil->video) }}">
                <div class="form-group mt-2" style="max-width: 20rem;">
                    <iframe width="500" height="280" src="{{ $profil->video }}" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
              </div>
              
              <button type="submit" class="btn btn-gradient-primary mr-2">Submit</button>
              <button class="btn btn-light">Cancel</button>
            </form>
          </div>
        </div>
      </div>
  </div>
  
    
@endsection