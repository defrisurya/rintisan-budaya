@extends('layouts.purple')

@section('content')
<div class="row" id="proBanner">
</div>


  <div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">
            <div class="row mb-4">
                <div class="col-md-6">
                    <h4 class="card-title">Data Anggota Struktur</h4>
                </div>
                <div class="col-md-6 text-right">
                    <a href="{{ route('detailstruktur.create') }}" class="btn btn-gradient-primary btn-sm">Add Data <i class="mdi mdi-loupe btn-icon-append"></i></a>
                </div>
            </div>
            <table class="table table-hover text-center">
              <thead>
                <tr>
                  <th> No </th>
                  <th> Foto </th>
                  <th> Deskripsi </th>
                  <th> Aksi </th>
                </tr>
              </thead>
              <tbody>
                  @forelse ($data as $item => $detailstruktur)
                  <tr>
                    <td class="py-1">
                        {{ $data->firstItem() + $item }}
                    </td>
                    <td>
                        <img src="{{ asset($detailstruktur->foto) }}" style="border-radius: 0; width: 150px; height: 100px;" alt="">
                    </td>
                    <td>
                        {!! \Illuminate\Support\Str::limit($detailstruktur->deskripsi, 55, $end='...') !!}
                    </td>
                    <td>
                      <form method="POST" action="{{ route('detailstruktur.destroy', $detailstruktur) }}" onsubmit="return confirm('Hapus Data, Anda Yakin ?')">
                        @method('DELETE')
                        @csrf
                        <a class="btn btn-gradient-warning btn-sm" href="{{ route('detailstruktur.edit', $detailstruktur) }}"><i class="mdi mdi-file-check btn-icon-append"></i></a>
                        {{-- <a class="btn btn-icon btn-success" href="{{ route('detailstruktur.show', $detailstruktur) }}"><i class="far fa-eye"></i></a> --}}
                        <button class="btn btn-gradient-danger btn-sm"><i class="mdi mdi-delete"></i></button>
                </form>
                    </td>
                  </tr>
                  @empty
                      <tr>
                        <td colspan="5">Data Masih Kosong</td>
                      </tr>
                  @endforelse
                
              </tbody>
            </table>
          </div>
          {{ $data->links() }}
        </div>
      </div>
      
  </div>
    
@endsection