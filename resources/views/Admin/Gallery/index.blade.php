@extends('layouts.purple')

@section('content')
<div class="row" id="proBanner">
    {{-- <div class="col-12">
      <span class="d-flex align-items-center purchase-popup">
        <p>Get tons of UI components, Plugins, multiple layouts, 20+ sample pages, and more!</p>
        <a href="https://www.bootstrapdash.com/product/purple-bootstrap-admin-template?utm_source=organic&utm_medium=banner&utm_campaign=free-preview" target="_blank" class="btn download-button purchase-button ml-auto">Upgrade To Pro</a>
        <i class="mdi mdi-close" id="bannerClose"></i>
      </span>
    </div> --}}
  </div>
  {{-- <div class="page-header">
    <h3 class="page-title">
      <span class="page-title-icon bg-gradient-primary text-white mr-2">
        <i class="mdi mdi-account-box"></i>
      </span> Gallery
    </h3>
    <nav aria-label="breadcrumb">
      <ul class="breadcrumb">
        <li class="breadcrumb-item active" aria-current="page">
          <span></span>Overview <i class="mdi mdi-alert-circle-outline icon-sm text-primary align-middle"></i>
        </li>
      </ul>
    </nav>
  </div> --}}

  <div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">
            <div class="row mb-4">
                <div class="col-md-6">
                    <h4 class="card-title">Gallery RKB</h4>
                </div>
                <div class="col-md-6 text-right">
                    <a href="{{ route('gallery.create') }}" class="btn btn-gradient-primary btn-sm">Add Data <i class="mdi mdi-loupe btn-icon-append"></i></a>
                </div>
            </div>
            <table class="table table-hover text-center">
              <thead>
                <tr>
                  <th> No </th>
                  <th> Judul </th>
                  <th> Foto </th>
                  <th> Aksi </th>
                </tr>
              </thead>
              <tbody>
                  @forelse ($data as $item => $gallery)
                  <tr>
                    <td class="py-1">
                        {{ $data->firstItem() + $item }}
                    </td>
                    <td> {{ $gallery->judul }} </td>
                    <td> <img src="{{ asset($gallery->foto) }}" style="border-radius: 0; width: 150px; height: 100px;" alt=""> </td>
                    <td>
                      <form method="POST" action="{{ route('gallery.destroy', $gallery) }}" onsubmit="return confirm('Hapus Data, Anda Yakin ?')">
                        @method('DELETE')
                        @csrf
                        <a class="btn btn-gradient-warning btn-sm" href="{{ route('gallery.edit', $gallery) }}"><i class="mdi mdi-file-check btn-icon-append"></i></a>
                        {{-- <a class="btn btn-icon btn-success" href="{{ route('gallery.show', $gallery) }}"><i class="far fa-eye"></i></a> --}}
                        <button class="btn btn-gradient-danger btn-sm"><i class="mdi mdi-delete"></i></button>
                </form>
                    </td>
                  </tr>
                  @empty
                      <tr>
                        <td colspan="4">Data Masih Kosong</td>
                      </tr>
                  @endforelse
                
              </tbody>
            </table>
          </div>
          {{ $data->links() }}
        </div>
      </div>
      
  </div>
    
@endsection