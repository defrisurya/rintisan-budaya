@extends('layouts.app')

@section('title', 'Kegiatan Rutin')

@section('content')
    <div id="preloader"></div>

    <!-- Navbar -->
    <header>
        <div id="sticker" class="header-area" style="background-color: rgba(0, 0, 0, 0.74)">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <nav class="navbar navbar-default" data-aos="fade-down" data-aos-duration="1200">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".bs-example-navbar-collapse-1" aria-expanded="false">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <a class="navbar-brand page-scroll sticky-logo" href="{{ route('welcome') }}">
                                    <img src="{{asset('image/Rectangle1.png')}}">
                                    <span>Dinas Kebudayaan (Kundha Kabudayan)<br>&nbsp;&nbsp;Kota Yogyakarta</span>
                                </a>
                            </div>
                            <div class="collapse navbar-collapse main-menu bs-example-navbar-collapse-1" id="navbar-example">
                                <ul class="nav navbar-nav navbar-right">
                                    <li>
                                        <a href="{{ route('welcome') }}">Home</a>
                                    </li>
                                    <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Profiles&nbsp;<span class="caret"></span></a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="{{ route('profile') }}">Profil Kampung</a></li>
                                            <li><a href="{{ route('struktur') }}">Struktur Organisasi</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="{{ route('gallery') }}">Gallery</a>
                                    </li>
                                    <li>
                                        <a href="#contact">Contact</a>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- End Navbar -->

    <div id="about" class="about-area area-padding">
        <div class="container">
            <div class="row" data-aos="fade-down" data-aos-duration="1200">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="section-headline text-center">
                        <h2><img class="group_11" src="{{ asset('image/Group_11.png') }}">&nbsp;&nbsp;Dolanan 1</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-sm-6 col-xs-12" data-aos="fade-right" data-aos-duration="1500" data-aos-delay="900">
                    <div class="well-left">
                        <div class="single-well">
                            <img src="{{ asset('image/poster.png') }}">
                        </div>
                    </div>
                </div>
                <div class="col-md-9 col-sm-6 col-xs-12" data-aos="fade-left" data-aos-duration="1500" data-aos-delay="700">
                    <div class="well-middle">
                        <div class="single-well">
                            <p>
                                Lorem ipsum dolor, sit amet consectetur adipisicing elit. Deserunt temporibus vero iusto illum recusandae, quae at culpa numquam cum voluptatibus ex? Nihil soluta excepturi at dicta cum! Magnam, suscipit ducimus?
                                Non dolorum nam, qui fuga recusandae soluta aliquid ipsam mollitia magni, voluptatibus rem tenetur odit facere dolor eos molestias cumque deserunt, saepe ratione error blanditiis quod. Distinctio earum voluptatibus harum.
                                Sed molestiae corrupti et maxime libero nesciunt assumenda atque. Pariatur perferendis, natus quam inventore quidem ducimus iusto ab nam dolorem ea maxime eius dolores atque qui. Omnis velit voluptas veniam!
                                Velit animi repudiandae natus consequuntur esse error unde id ad voluptatibus, ipsam eaque repellendus ab temporibus quisquam expedita minima sint, illum at, iste neque. Labore obcaecati illo id tenetur corrupti?
                                Itaque, amet quod recusandae veritatis eos quas quidem exercitationem est! Hic dolores quibusdam soluta eveniet explicabo. Iure, debitis? Eveniet veritatis alias eius eos fugiat corporis nihil obcaecati saepe molestiae a!
                                Voluptatum placeat fugit maiores qui debitis quae rerum ipsa nisi pariatur nobis consequatur voluptas odio obcaecati veritatis laboriosam itaque corrupti veniam, nulla commodi eius temporibus impedit. Nisi odio in reiciendis.
                                Repellendus, laborum aut error nihil cum consequuntur, placeat magnam ab corporis veritatis commodi nobis aspernatur, quos perspiciatis? Quis quisquam assumenda quibusdam tenetur maiores quos deleniti unde, sit commodi, excepturi non?
                                Neque voluptates vel error iusto laborum esse alias harum. Error porro repudiandae nobis debitis cumque illum ut enim? Asperiores vel soluta, doloribus molestiae sunt voluptas temporibus sint ducimus est explicabo?
                                In maiores soluta, hic nobis doloremque, non iusto debitis eligendi nulla praesentium aspernatur consectetur maxime ea accusantium ab ullam facere tempore eveniet veritatis. Necessitatibus libero dicta alias praesentium! Iusto, molestias.
                                Excepturi commodi reprehenderit, fugiat enim quibusdam assumenda modi dignissimos quas est, vitae autem. Incidunt expedita beatae molestiae soluta veritatis fugit sunt dolorum, dolore reiciendis, vero consectetur in nobis dolorem perspiciatis.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('layouts.components.footer')
@endsection
@section('script')
    <script type="text/javascript">
        /*AOS
        --------------------- */
        AOS.init();
    </script>
@endsection
