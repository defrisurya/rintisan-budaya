<footer id="contact">
    <div class="footer-area">
        <div class="container">
            @php
                $prof = App\Profil::first();
            @endphp
            <div class="row">
                <div class="col-md-5 col-sm-4 col-xs-12">
                    <div class="footer-content">
                        <div class="footer-head">
                            <div class="footer-logo">
                                <h3>Kampung Rintisan Budaya</h3>
                            </div>
                            <div class="footer-logo">
                                <h2>Cokrodiningratan</h2>
                            </div>
                            <p>Alamat<br>{{ $prof->alamat }}</p>
                        </div>
                    </div>
                </div>
                <!-- end single footer -->
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="footer-content">
                        <div class="footer-head">
                            <h4>Contact</h4>
                            <div class="footer-contacts">
                                <p><i class="fa fa-envelope"></i>&nbsp;{{ $prof->email }}</p>
                                <p><i class="fa fa-phone"></i>&nbsp;{{ $prof->notelpon }}</p>
                                <p><i class="fa fa-whatsapp"></i>&nbsp;{{ $prof->notelpon }}</p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end single footer -->
                <div class="col-md-3 col-sm-4 col-xs-12">
                    <div class="footer-content">
                        <div class="footer-head">
                            <h4>Social Media</h4>
                            <div class="footer-icons">
                                <ul>
                                    <li>
                                        <a href="{{ $prof->fb }}" target="_blank"><i class="fa fa-facebook"></i></a>
                                    </li>
                                    <li>
                                        <a href="{{ $prof->ig }}" target="_blank"><i class="fa fa-instagram"></i></a>
                                    </li>
                                    <li>
                                        <a href="{{ $prof->ytb }}" target="_blank"><i class="fa fa-youtube"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <img src="{{ asset('front/image/FooterBottom.png') }}" style="bottom: 0px">
    </div>
    <div class="footer-area-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="copyright text-center">
                        <p>
                            &copy; Copyright <strong>PT Inovasi Nuswantara Digital</strong>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
