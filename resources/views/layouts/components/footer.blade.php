<footer id="contact" class="reveal">
    <div class="footer-area">
        <div class="container">
            <div class="row">
                <div class="col-md-5 col-sm-4 col-xs-12">
                    <div class="footer-content">
                        <div class="footer-head">
                            <div class="footer-logo">
                                <h3>Kampung Rintisan Budaya</h3>
                            </div>
                            <div class="footer-logo">
                                <h2>Cokrodiningratan</h2>
                            </div>
                            <p>Alamat<br>Jl. Kemasan No.39, Purbayan, Kec. Kotagede, Kota Yogyakarta,<br>Daerah Istimewa Yogyakarta 55173</p>
                        </div>
                    </div>
                </div>
                <!-- end single footer -->
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="footer-content">
                        <div class="footer-head">
                            <h4>Contact</h4>
                            <div class="footer-contacts">
                                <p><i class="fa fa-envelope"></i> cokrodiningratan@jogjakota.com</p>
                                <p><i class="fa fa-phone"></i> +628x-xxx-xxx</p>
                                <p><i class="fa fa-whatsapp"></i> 0877-6546-9874</p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end single footer -->
                <div class="col-md-3 col-sm-4 col-xs-12">
                    <div class="footer-content">
                        <div class="footer-head">
                            <h4>Social Media</h4>
                            <div class="footer-icons">
                                <ul>
                                    <li>
                                        <a href="#"><i class="fa fa-facebook"></i></a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="fa fa-instagram"></i></a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="fa fa-youtube"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <img src="{{ asset('image/FooterBottom.png') }}" style="bottom: 0px">
    </div>
    <div class="footer-area-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="copyright text-center">
                        <p>
                            &copy; Copyright <strong>PT Inovasi Nuswantara Digital</strong>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
