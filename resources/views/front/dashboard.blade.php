@extends('layouts.front.app')

@section('title', 'Kampung Rintisan Budaya | Cokrodiningratan')

@section('content')
    <div id="preloader"></div>

    <!-- Navbar -->
    @include('layouts.front.components.navbar')

    <!-- Video -->
    <div id="main" class="container-video">
        <div id="video" class="video">
            <iframe src="{{ $rkb->video }}"frameborder="" allowfullscreen></iframe>
        </div>
    </div>
    {{-- <div class="page-content">
        <a href="javascript:void(0)" class="play-btn">
            <img class="play-img" src="{{ asset('front/image/play.png') }}">
            <img class="pause-img" src="{{ asset('front/image/pause.png' ) }}">
        </a>
    </div> --}}
    <!-- End Video -->

    <!-- Profil Kampung -->
    <div id="about" class="about-area area-padding reveal">
        <div class="container">
            <div class="row" data-aos="fade-down" data-aos-offset="400" data-aos-duration="1200">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="section-headline text-center">
                        <h2><img class="group_11" src="{{ asset('front/image/Group_11.png') }}">&nbsp;&nbsp;Kampung Cokrodiningratan</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-sm-6 col-xs-12" data-aos="fade-up" data-aos-duration="1500" data-aos-delay="900">
                    <div class="well-left">
                        <div class="single-well">
                            <img src="{{ asset($rkb->foto) }}">
                        </div>
                    </div>
                </div>
                <div class="col-md-8 col-sm-6 col-xs-12" data-aos="fade-up" data-aos-duration="1500" data-aos-delay="700">
                    <div class="well-middle">
                        <div class="single-well">
                        <p>{{ $rkb->deskripsi }}</p>
                        <p>
                            <a href="{{ route('profile') }}">Selengkapnya</a>
                        </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Profil Kampung -->

    <!-- Kegiatan Rutin -->
    <div id="services" class="services-area area-padding reveal">
        <div id="content-rutin">
            <div class="head" data-aos="fade-down" data-aos-duration="1200">
                <h2><img class="group_12" src="{{ asset('front/image/Group_11.png') }}">&nbsp;Kegiatan Rutin</h2>
            </div>
            <img class="group_10" src="{{ asset('front/image/Group10.png') }}" data-aos="fade-right" data-aos-duration="1200">
            <div class="slider" data-aos="zoom-in" data-aos-duration="1200" data-aos-delay="900">
                @foreach ( $kr as $item )
                    <div class="slide-box">
                        <img class="img-kr" src="{{ asset($item->foto) }}">
                        <h4>{{$item->judul}}</h4>
                        <p>{!! $item->deskripsi !!}</p>
                        <a href="{{ url('kegiatanrutin', $item) }}">
                            Read More&nbsp;<i class="fa fa-arrow-circle-right"></i>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <!-- End Kegiatan Rutin -->

    <!-- Gallery Kegiatan -->
    <div id="portfolio" class="portfolio-area area-padding fix reveal">
        <div class="container">
            <div class="row" data-aos="fade-down" data-aos-duration="1200" data-aos-offset="400">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="section-headline text-center">
                        <h2><img class="group_13" src="{{ asset('front/image/Group_11.png') }}">&nbsp;Gallery Kegiatan</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="awesome-project-content">
                    @foreach ($gal as $gallery)
                        <div class="col-md-3 col-sm-4 col-xs-12 design development" data-aos="flip-left" data-aos-easing="ease-out-cubic" data-aos-duration="1200">
                            <div class="single-awesome-project">
                                <div class="awesome-img">
                                    <a href="#"><img class="image" src="{{asset($gallery->foto)}}" /></a>
                                    <div class="add-actions text-center">
                                        <div class="project-dec">
                                            <a class="venobox" data-gall="myGallery" href="{{asset($gallery->foto)}}">
                                                <h4>{{$gallery->judul}}</h4>
                                                <p>Lihat Gambar&nbsp;<i class="fa fa-arrow-circle-right"></i></p>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="read-more" data-aos="fade-up" data-aos-duration="1200">
                <a href="{{ url('gallerykegiatan') }}">Selengkapnya</a>
            </div>
        </div>
    </div>
    <!-- End Gallery Kegiatan -->

    <!-- Peta Lokasi -->
    <div class="contact-area reveal">
        <div class="contact-inner area-padding">
            <div class="contact-overly"></div>
            <div class="container">
                <div class="row" data-aos="fade-down" data-aos-duration="1200">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="section-headline text-center">
                            <h2><img class="group_14" src="{{ asset('front/image/Group_11.png') }}">&nbsp;Peta Lokasi</h2>
                        </div>
                    </div>
                </div>
                <div class="row" data-aos="fade-up" data-aos-anchor-placement="top-bottom" data-aos-duration="1200">
                    <!-- Google Map -->
                    <div class="col-md-12 col-sm-6 col-xs-12">
                        <!-- Map -->
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3952.689847534188!2d110.39872101415472!3d-7.822615379857391!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e7a571635557107%3A0x5e4aca81682eac02!2sJl.%20Kemasan%20No.39%2C%20Purbayan%2C%20Kec.%20Kotagede%2C%20Kota%20Yogyakarta%2C%20Daerah%20Istimewa%20Yogyakarta%2055173!5e0!3m2!1sen!2sid!4v1635773610915!5m2!1sen!2sid" width="100%" height="380" frameborder="0" style="border:1px solid rgba(0, 0, 0, 0.40)" allowfullscreen></iframe>
                        <!-- End Map -->
                    </div>
                    <!-- End Google Map -->
                </div>
            </div>
        </div>
    </div>
    <!-- End Peta Lokasi -->

    <!-- Footer -->
    @include('layouts.front.components.footer')
@endsection

@section('script')
    <script type="text/javascript">
        /*Video Player
        --------------------- */
        $(document).ready(function () {
            $(".play-btn .play-img").on("click", function () {
                $(".play-btn .play-img").fadeOut();
                $(".play-btn .pause-img").fadeIn();
                $("#video")[0].play();
            });

            $(".play-btn .pause-img").on("click", function () {
                $(".play-btn .play-img").fadeIn();
                $(".play-btn .pause-img").fadeOut();
                $("#video")[0].pause();
            });

            // when video is play
            $("#video")[0].onplay = function () {
                $(".play-btn .play-img").fadeOut();
                $(".play-btn .pause-img").fadeIn();
            };

            // and video is end
            $("#video")[0].onended = function () {
                $(".play-btn .play-img").fadeIn();
                $(".play-btn .pause-img").fadeOut();
            };

            $(".toggle").on("click", function () {
                $(".toggle-content").slideToggle();
            });
        });
    </script>

    <script type="text/javascript">
        /*Slick Slider
        --------------------- */
        $(document).ready(function(){
            $('.slider').slick({
                centerMode: true,
                centerPadding: '60px',
                slidesToShow: 1,
                speed:1000,
                // index: 2,
                responsive: [
                    {
                    breakpoint: 768,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        centerPadding: '40px',
                        slidesToShow: 1
                    }
                    },
                    {
                    breakpoint: 480,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        centerPadding: '40px',
                        slidesToShow: 1
                    }
                    }
                ]
                });
        });
    </script>

    <script type="text/javascript">
        /*AOS
        --------------------- */
        AOS.init();
    </script>
@endsection
