@extends('layouts.front.app')

@section('title', 'Gallery Kegiatan')

@section('content')
    <div id="preloader"></div>

    <!-- Navbar -->
    <header>
        <div id="sticker" class="header-area" style="background-color: rgba(0, 0, 0, 0.74)">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <nav class="navbar navbar-default" data-aos="fade-down" data-aos-duration="1200">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".bs-example-navbar-collapse-1" aria-expanded="false">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <a class="navbar-brand page-scroll sticky-logo" href="{{ route('rkb') }}">
                                    <img src="{{asset('front/image/Rectangle1.png')}}">
                                    <span>Dinas Kebudayaan (Kundha Kabudayan)<br>&nbsp;&nbsp;Kota Yogyakarta</span>
                                </a>
                            </div>
                            <div class="collapse navbar-collapse main-menu bs-example-navbar-collapse-1" id="navbar-example">
                                <ul class="nav navbar-nav navbar-right">
                                    <li>
                                        <a href="{{ route('rkb') }}">Home</a>
                                    </li>
                                    <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Profiles&nbsp;<span class="caret"></span></a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="{{ route('profile') }}">Profil Kampung</a></li>
                                            <li><a href="{{ route('struktur') }}">Struktur Organisasi</a></li>
                                        </ul>
                                    </li>
                                    <li class="active">
                                        <a href="{{ url('gallerykegiatan') }}">Gallery</a>
                                    </li>
                                    <li>
                                        <a href="#contact">Contact</a>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- End Navbar -->

    <!-- Show All Gallery -->
    <div id="portfolio" class="portfolio-area area-padding fix">
        <div class="container">
            <div class="row" data-aos="fade-down" data-aos-duration="1200">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="section-headline text-center">
                        <h2><img class="group_13" src="{{ asset('front/image/Group_11.png') }}">&nbsp;Gallery Kegiatan</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="awesome-project-content">
                    @foreach ($galeri as $gallery)
                        <div class="col-md-4 col-sm-4 col-xs-12 design development" data-aos="flip-left" data-aos-easing="ease-out-cubic" data-aos-duration="1200" data-aos-delay="900">
                            <div class="single-awesome-project">
                                <div class="awesome-img">
                                    <a href="#"><img class="image-img" src="{{asset($gallery->foto)}}" alt="" /></a>
                                    <div class="add-actions text-center">
                                        <div class="project-dec">
                                            <a class="venobox" data-gall="myGallery" href="{{asset($gallery->foto)}}">
                                                <h4>{{$gallery->judul}}</h4>
                                                <p>Lihat Gambar&nbsp;<i class="fa fa-arrow-circle-right"></i></p>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <!-- End Show All Gallery -->

    @include('layouts.front.components.footer')
@endsection

@section('script')
    <script type="text/javascript">
        /*AOS
        --------------------- */
        AOS.init();
    </script>
@endsection
